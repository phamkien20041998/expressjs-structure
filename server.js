const express = require('express');
const cors = require('cors');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('./swagger');
const { responseEnhancer } = require('express-response-formatter');
const db = require('./src/config/sequelize');
const corsOptions = {
    origin: 'http://localhost:8081'
};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: 1 }));
app.use(responseEnhancer());
// simple route
app.get('/', (req, res) => {
    res.json({ message: 'Welcome to bezkoder application.' });
});

require('./src/routes/postRoute')(app);

// set port, listen for requests
const PORT = process.env.PORT || 4000;

const initApp = async () => {
    try {
        await db.sequelize.authenticate();
        // eslint-disable-next-line no-console
        console.log('Connection has been established successfully.');

        app.listen(PORT, () => {
            // eslint-disable-next-line no-console
            console.log(`Server is running on port ${PORT}.`);
        });
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error('Unable to connect to the database:', error.original);
    }
};

initApp();
