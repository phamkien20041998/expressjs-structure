/**
 * @swagger
 * /api/posts:
 *   get:
 *     summary: Get a post of users
 *     description: Retrieve a list of users from the database
 *     responses:
 *       200:
 *         description: A list of users
 */
