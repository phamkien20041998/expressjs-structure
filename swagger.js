const swaggerJSDoc = require('swagger-jsdoc');

// http://localhost:4000/api-docs
const options = {
    swaggerDefinition: {
        info: {
            title: 'Express API',
            version: '1.0.0',
            description: 'API documentation for your Express.js application'
        }
    },
    apis: ['./swagger/*.js']
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = swaggerSpec;
