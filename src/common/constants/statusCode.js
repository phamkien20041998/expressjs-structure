const HTTP_STATUS_CODE = {
    C200: 200,
    C204: 204,
    C400: 400,
    C401: 401,
    C403: 403,
    C404: 404,
    C500: 500
};

module.exports = { HTTP_STATUS_CODE };
