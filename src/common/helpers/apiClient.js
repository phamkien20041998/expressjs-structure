const axios = require('axios');

const get = (path, params, token) => axios.create({
    timeout: 1000,
    headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
}).get(path, params)
    .then(response => response.data);

const post = (path, params, token) => axios.create({
    timeout: 1000,
    headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
}).post(path, params)
    .then(response => response.data);

module.exports = { get, post };
