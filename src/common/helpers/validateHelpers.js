const Joi = require('joi');

const title = Joi.string().min(5).required();
const content = Joi.string().min(5).required();

module.exports = {
    Joi,
    title,
    content
};
