const winston = require('winston');
const fs = require('fs');
const path = require('path');

const dynamicFileName = (logFile) => {
    const logDir = 'logs';
    if (!fs.existsSync(logDir)) {
        fs.mkdirSync(logDir);
    }
    const currentDate = new Date().toISOString().slice(0, 10);

    return path.join(logDir, `${logFile}-${currentDate}.log`);
};

const logger = winston.createLogger(
    {
        level: 'info',
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.errors({ stack: true }),
            winston.format.printf(({ timestamp, level, message, stack }) => {
                if (stack) {
                    return `[${timestamp}] ${level}: ${message}\n${stack}`;
                }
                return `[${timestamp}] ${level}: ${message}`;

            })
        ),
        transports: [
            new winston.transports.Console(),
            new winston.transports.File({
                filename: dynamicFileName('error'),
                level: 'error'
            }),
            new winston.transports.File({
                filename: dynamicFileName('debug'),
                level: 'debug'
            })
        ]
    }
);

/*
 * Nếu bạn muốn cũng ghi log vào console
 * if (process.env.NODE_ENV !== 'production') {
 *     logger.add(new winston.transports.Console({
 *         format: winston.format.simple(),
 *     }));
 * }
 */

module.exports = logger;
