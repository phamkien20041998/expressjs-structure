const env = require('../../env');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.DB, env.USER, env.PASSWORD, {
    host: env.HOST,
    port: env.PORT,
    dialect: env.dialect,
    operatorsAliases: 0,

    pool: {
        max: env.pool.max,
        min: env.pool.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

const model = {};

model.Sequelize = Sequelize;
model.sequelize = sequelize;

model.post = require('../models/post')(sequelize, Sequelize);
model.post.sync();
module.exports = model;
