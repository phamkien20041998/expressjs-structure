module.exports = app => {
    const postController = require('../controllers/postController.js');
    const { middlewareAuth } = require('../middleware/authentication');

    var router = require('express').Router();

    router.get('/user/laravel', middlewareAuth, postController.getUserFromLaravel);
    router.post('/user/laravel', middlewareAuth, postController.createUserFromLaravel);
    router.post('/', postController.create);
    router.get('/', postController.findAll);
    router.get('/:id', postController.findOne);
    router.put('/:id', postController.update);
    router.delete('/:id', postController.deleteOne);
    router.delete('/', postController.deleteAll);

    app.use('/api/v1', router);
};
