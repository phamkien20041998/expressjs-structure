const model = require('../config/sequelize');
const apiClient = require('../common/helpers/apiClient');
const { GET_USER_PATH, POST_USER_PATH } = require('../common/constants/endPoint');
const postModel = model.post;
const Op = model.Sequelize.Op;
const logger = require('../config/logging');
const { validator } = require('../validation/postValidate');

// get user from laravel virtual
const getUserFromLaravel = async (req, res) => {
    try {
        const params = {};
        const data = await apiClient.get(GET_USER_PATH, params, req.token);

        res.formatter.ok(data);
        // return successResponse(res, data);
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

// get user from laravel virtual
const createUserFromLaravel = async (req, res) => {
    const params = req.body;
    const data = await apiClient.post(POST_USER_PATH, params, req.token);

    res.formatter.ok(data);
};

// get all data
const findAll = async (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
    const validate = validator({title: title});

    if (validate.error) {
        res.formatter.badRequest(validate.error);
    }
    try {
        const data = await postModel.findAll({ where: condition });

        res.formatter.ok(data);
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

// find one
const findOne = async (req, res) => {
    try {
        const id = req.params.id;
        const data = await postModel.findByPk(id);
        if (!data) {
            res.formatter.notFound('Item not found');
        }

        res.formatter.ok(data);
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

// create item
const create = async (req, res) => {
    try {
        const post = {
            title: req.body.title,
            content: req.body.content,
            status: req.body.status ? req.body.status : true
        };

        const data = await postModel.create(post);

        res.formatter.created(data.dataValues);
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

// update item
const update = async (req, res) => {
    try {
        const id = req.params.id;
        const isUpdate = await postModel.update(req.body, {
            where: { id: id }
        });

        if (isUpdate[0] === 1) {
            res.formatter.ok([]);
        }
        res.formatter.badRequest('failed to update');
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

// delete one
const deleteOne = async (req, res) => {
    try {
        const id = req.params.id;
        const isDelete = await postModel.destroy({
            where: { id: id }
        });

        if (isDelete[0] === 1) {
            res.formatter.ok([]);
        }
        res.formatter.badRequest('failed to delete');
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

// delete all
const deleteAll = async (req, res) => {
    try {
        const data = await postModel.destroy({
            where: {},
            truncate: 0
        });
        res.formatter.ok(data);
    } catch (err) {
        res.formatter.serverError(err.message);
    }
};

module.exports = {
    create,
    findAll,
    findOne,
    update,
    deleteOne,
    deleteAll,
    getUserFromLaravel,
    createUserFromLaravel
};
