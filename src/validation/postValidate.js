const { Joi, title, content } = require('../common/helpers/validateHelpers');

const schema = Joi.object().keys({
    title: title,
    content: content
});

const validator = (data) => schema.validate(data, {
    abortEarly: false
});

module.exports = {
    validator
};
