const middlewareAuth = (req, res, next) => {
    if (req.headers.authorization.split(' ')[1] === 'xxxx' && req.headers.authorization.split(' ')[0] === 'Bearer') {
        req.token = req.headers.authorization.split(' ')[1];
        next();
    } else {
        res.formatter.unauthorized('Unauthorized');
    }
};

module.exports = { middlewareAuth };
