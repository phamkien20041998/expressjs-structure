# Node.js Rest APIs with Express, Sequelize & MySQL example
## Project setup
```
npm install
```
```
npm start
```
## Working with Model
### _1. Create model using sequelize_
```
npx sequelize-cli model:generate --name <name> --attributes <field_name>:<data_type>,...
```

> Note:
> `name` is model name.
> `field_name` is field of model.
> `data_type` is data type of field.

### _2. Migrate_
```
npx sequelize-cli db:migrate
```

### _3. Seeder_
```
npx sequelize-cli seed:generate --name <name>
```
> Note:
> `name` is name seeder.


https://www.npmjs.com/package/express-response-formatter